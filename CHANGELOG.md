## [1.2.3](https://gitlab.com/mlaplanche/backdata/compare/v1.2.2...v1.2.3) (2022-11-22)


### Bug Fixes

* **cicd:** update docker hub readme ([3ea31e9](https://gitlab.com/mlaplanche/backdata/commit/3ea31e9c5d8d4570570f6e051cc5b36f143369ff))
* **envvars:** change backup cron environment variable name ([af9c281](https://gitlab.com/mlaplanche/backdata/commit/af9c28186c83b321a83f4929501beb108f2fb888))

## [1.2.2](https://gitlab.com/mlaplanche/backdata/compare/v1.2.1...v1.2.2) (2022-11-18)


### Bug Fixes

* **cicd:** update docker hub readme ([0651bbe](https://gitlab.com/mlaplanche/backdata/commit/0651bbe0963cfadb90754f0d18fb237daf93b710))

## [1.2.1](https://gitlab.com/mlaplanche/backdata/compare/v1.2.0...v1.2.1) (2022-11-18)


### Bug Fixes

* **cicd:** try to fix docker hub connection ([f4dff91](https://gitlab.com/mlaplanche/backdata/commit/f4dff91a6d57ac793c941fab7c8c091050f6dd0b))

# [1.2.0](https://gitlab.com/mlaplanche/backdata/compare/v1.1.0...v1.2.0) (2022-11-18)


### Features

* **cicd:** add push to docker hub ([be616ef](https://gitlab.com/mlaplanche/backdata/commit/be616ef781052467ca5bf57d22a1d23ec70b2311))

# [1.1.0](https://gitlab.com/mlaplanche/backdata/compare/v1.0.5...v1.1.0) (2022-11-18)


### Bug Fixes

* **cicd:** add zip to dotnet:build ([d7d5896](https://gitlab.com/mlaplanche/backdata/commit/d7d5896f77ae10c83067e181eac749f1f258dd35))


### Features

* **cicd:** zip published app ([ca3761d](https://gitlab.com/mlaplanche/backdata/commit/ca3761dcff5cabb77441e68e2890abdf790810ba))

## [1.0.5](https://gitlab.com/mlaplanche/backdata/compare/v1.0.4...v1.0.5) (2022-11-18)


### Bug Fixes

* **cicd:** try to fix assets in release ([33b0695](https://gitlab.com/mlaplanche/backdata/commit/33b0695c8cf2ffdde19642d4349e6a1789f9c965))

## [1.0.4](https://gitlab.com/mlaplanche/backdata/compare/v1.0.3...v1.0.4) (2022-11-18)


### Bug Fixes

* **ci/cd:** add image for build dotnet ([94a39d9](https://gitlab.com/mlaplanche/backdata/commit/94a39d9f0e2b8fcdee2bd9184fb52eefca344e9f))
* **ci/cd:** dotnet build error ([d3f998a](https://gitlab.com/mlaplanche/backdata/commit/d3f998a95b08e7496425b994d767b7654a775465))
* **cicd:** add release gitlab ([46db8cf](https://gitlab.com/mlaplanche/backdata/commit/46db8cf7707a59c5db6b3cd0f158f8d0b15c8085))
* **cicd:** add release gitlab ([1ed029a](https://gitlab.com/mlaplanche/backdata/commit/1ed029a2c0c9d15d3741e59d6ef8cf8c9524ce42))
* **cicd:** fix configuration on publish ([74af100](https://gitlab.com/mlaplanche/backdata/commit/74af10008939281424e90bb7594d3c8eb688a54f))
* **cicd:** fix dotnet build output ([1cb1153](https://gitlab.com/mlaplanche/backdata/commit/1cb11535f6fde9be08dd997bcfd84068a8871f33))
