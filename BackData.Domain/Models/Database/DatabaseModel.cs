using YamlDotNet.Serialization;

namespace BackData.Domain.Models.Database;

public class DatabaseModel
{
    public string Host { get; set; } = default!;

    public string DatabaseName { get; set; } = default!;

    public string UserName { get; set; } = default!;

    public string Password { get; set; } = default!;

    public string Port { get; set; } = default!;
    public string CronSchedule { get; set; } = "0 0 12,0 * * ?";
    public string TypeDatabaseString { get; set; } = default!;

    [YamlIgnore]
    public TypeDatabaseEnum TypeDatabase
    {
        get => GetTypeDatabaseFromString(TypeDatabaseString);
        set => TypeDatabase = value;
    }

    [YamlIgnore] public string JobName => $"{DatabaseName}.{_guid}";
    [YamlIgnore] private readonly Guid _guid = Guid.NewGuid();
    public static TypeDatabaseEnum GetTypeDatabaseFromString(string typeDatabase)
    {
        return typeDatabase switch
        {
            "mysql" => TypeDatabaseEnum.Mysql,
            "postgresql" => TypeDatabaseEnum.PostgreSql,
            "sqlserver" => TypeDatabaseEnum.SqlServer,
            _ => throw new ApplicationException("Unknown database type")
        };
    }
}