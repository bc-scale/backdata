﻿// See https://aka.ms/new-console-template for more information

using BackData.Extensions;
using BackData.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quartz;

var builder = Host.CreateDefaultBuilder(args)
    .ConfigureLogging(loggingBuilder =>
    {
        loggingBuilder.ClearProviders();
        loggingBuilder.AddLog4Net();
    })
    .ConfigureAppConfiguration(configurationBuilder =>
    {
        configurationBuilder.AddEnvironmentVariables();
        configurationBuilder.AddJsonFile("appsettings.json");
    })
    .ConfigureServices((cxt, services) =>
    {
        services.AddQuartz(q => { q.UseMicrosoftDependencyInjectionJobFactory(); });
        services.AddQuartzHostedService(opt => { opt.WaitForJobsToComplete = true; });
        services.AddBackData();
    }).Build();

var schedulerFactory = builder.Services.GetRequiredService<ISchedulerFactory>();

await BackupService.RunAllBackUp(schedulerFactory);

Console.ReadLine();