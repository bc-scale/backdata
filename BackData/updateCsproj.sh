# !/bin/bash
# Written by rafazzevedo
# http://www.azevedorafaela.wordpress.com

# Uncomment this if you want to use parameters given by #the user and change the variables for the parameters #position like: $1 is the first parameter, $2 is the #second. Instead of tag, xml_file...
# if [ $# -ne 3 ]; then
# echo 1>&2 "Please, use the parameters file, tag and new value."
# exit 127
# fi

tag="amount"
new_value="\$1000,000.00"

# We will create a temporary file, just to not modify directly the original one.
temporary="temp_file.temp"

# This space is just to identify the end of the xml.
echo " ">> "./BackData.csproj"

# Extracting the value from the <Version> element
tag_value=$(grep "<Version>.*<.Version>" "./BackData.csproj" | sed -e "s/^.*<Version/<Version/" | cut -f2 -d">"| cut -f1 -d"<")

echo "Found tag value $tag_value..."

# Replacing element value with $new_value
sed -e "s/<Version>$tag_value<\/Version>/<Version>$new_value<\/Version>/g" "./BackData.csproj" > $temporary

echo "Changing Version to $new_value..."

# Updating the changes to the original file ("./BackData.csproj")
chmod 666 "./BackData.csproj"
mv $temporary "./BackData.csproj"