using BackData.Domain.Models.Database;
using BackData.Services;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Quartz;

namespace BackData.Jobs;

/// <summary>
/// MysqlJob for Quartz 
/// </summary>
public class MysqlJob : IJob
{
    private readonly ILogger<MysqlJob> _logger;

    public MysqlJob(ILogger<MysqlJob> logger)
    {
        _logger = logger;
    }

    public Task Execute(IJobExecutionContext context)
    {
        var loggerIdentifier = context.Trigger.Key;
        var database = (DatabaseModel)context.MergedJobDataMap["database"];
        _logger.LogInformation("");
        _logger.LogInformation("###################################################");
        _logger.LogInformation("{DatabaseJobName} - MySQL", loggerIdentifier);

        var connectionString = new MySqlConnectionStringBuilder
        {
            Database = database.DatabaseName,
            Server = database.Host,
            UserID = database.UserName,
            Password = database.Password,
            Port = uint.Parse(database.Port)
        };

        var connection = new MySqlConnection(connectionString.ToString());
        try
        {
            _logger.LogDebug("{DatabaseJobName} - Connecting to MySQL...", loggerIdentifier);
            connection.Open();
            _logger.LogDebug("{DatabaseJobName} - Connected to MySQL", loggerIdentifier); 
            using var cmd = new MySqlCommand { Connection = connection };
            using var backup = new MySqlBackup(cmd);
            var backupstr = backup.ExportToString();
            FileBackupService.CreateBackupFile(database, backupstr);
            // Perform database operations

            _logger.LogInformation("{DatabaseJobName} - Backup success", loggerIdentifier);
        }
        catch (Exception ex)
        {
            _logger.LogError("{S}", ex.ToString());
        }
        finally
        {
            connection.Close();
            _logger.LogInformation("{DatabaseJobName} - Connection close", loggerIdentifier);
        }
        _logger.LogInformation("{DatabaseJobName} - Job end", loggerIdentifier);
        return new Task(() => { });
    }
}