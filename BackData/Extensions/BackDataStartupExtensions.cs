using BackData.Domain.Models.Database;
using BackData.Models;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace BackData.Extensions;

public static class BackDataStartupExtensions
{
    private static readonly ILog Logger;

    static BackDataStartupExtensions()
    {
        Logger = LogManager.GetLogger(typeof(Program));
    }

    public static void AddBackData(this IServiceCollection services)
    {
        Logger.Info("BackData Start");
        InitializeFileConf();
        InitializeConf();
    }

    /// <summary>
    ///     Allows the implementation of the configuration file
    /// </summary>
    private static void InitializeConf()
    {
        var envVars = Environment.GetEnvironmentVariables();
        var confFile = new BackDataConfModel();

        var enableCompression = (bool)(envVars["ENABLE_COMPRESSION"] ?? false);

        var dbHost = (string?)envVars["DB_HOST"];
        var dbType = (string?)envVars["DB_TYPE"];
        var dbName = (string?)envVars["DB_NAME"];
        var dbUser = (string?)envVars["DB_USER"];
        var dbPass = (string?)envVars["DB_PASSWORD"];
        var dbBackupCron = (string?)envVars["DB_BACKUP_CRON"];
        var dbPort = (string?)envVars["DB_PORT"];

        // If configured by environment variables 
        if (dbHost is not null)
        {
            Logger.Warn("/!\\ Environment variables mode activated /!\\ ");
            confFile.EnableCompression = enableCompression;
            var database = new DatabaseModel
            {
                Host = dbHost
            };

            if (dbName is not null) database.DatabaseName = dbName;
            if (dbUser is not null) database.UserName = dbUser;
            if (dbPass is not null) database.Password = dbPass;
            if (dbPort is not null) database.Port = dbPort;
            if (dbType is not null) database.TypeDatabaseString = dbType;
            if (dbBackupCron is not null) database.CronSchedule = dbBackupCron;

            confFile.Databases.Add(database);
            Logger.Info($"{database.DatabaseName} added to configuration");
        }
        else
        {
            // the configuration file is already created
            Logger.Warn("/!\\ Config file mode activated /!\\ ");
            if (new FileInfo(BackDataGlobal.DefaultLocationConf).Length != 0) return;
        }

        // Creation of the configuration file
        var serializer = new SerializerBuilder().WithNamingConvention(CamelCaseNamingConvention.Instance).Build();
        File.WriteAllText(BackDataGlobal.DefaultLocationConf, serializer.Serialize(confFile));
    }

    /// <summary>
    ///     Allows the creation of the configuration file
    /// </summary>
    private static void InitializeFileConf()
    {
        Logger.Info($"Verify if {BackDataGlobal.DefaultLocationConf} exists.");
        if (!Directory.Exists(BackDataGlobal.DefaultDirectoryConf))
            Directory.CreateDirectory(BackDataGlobal.DefaultDirectoryConf);

        if (File.Exists(BackDataGlobal.DefaultLocationConf)) return;
        
        Logger.Warn($"File {BackDataGlobal.DefaultLocationConf} not found.");
        Logger.Warn($"Create {BackDataGlobal.DefaultLocationConf} file.");
        
        File.Create(BackDataGlobal.DefaultLocationConf).Close();
    }
}