using BackData.Domain.Models.Database;

namespace BackData.Models;

public class BackDataConfModel
{
    public bool EnableCompression { get; set; } = false;
    public IList<DatabaseModel> Databases { get; set; } = new List<DatabaseModel>();
}